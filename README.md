# Things to do

- Move all user properties to template
- Add more properties for database (description, image url, price, ...)
- Support multiple flavours?
- Seems we have too many direct questions!
- UI???

# Requirements

- Python 2.7 with Virtualenv (https://www.python.org/downloads/)

# Installation

```sh
# Clone this code folder
git clone ke4102/
cd ke4102/

# Setup virtualenv
virtualenv .venv/
source .venv/bin/activate

# Install libraries
pip install -r requirements.txt
```

# Run servers

```sh
# Run backend
gunicorn -w 3 main:app

# Run frontend (different commant prompt)
python -m SimpleHTTPServer 8080
```

Access: http://localhost:8080
You should see something like this:

![alt text](food_clips.png "App Screenshot")
