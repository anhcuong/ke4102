;==============================================================
;	Main: Templates and Facts
;==============================================================

(deftemplate MAIN::indicator
	(slot name)
	(slot status)
	(slot value))

(deftemplate MAIN::diet
	(multislot req))

(deftemplate MAIN::allergy
	(multislot is))

(deftemplate MAIN::selection
	(slot is))

(deftemplate MAIN::personality
	(slot is))

(deffacts MAIN:recommendation_status
	(indicator (name recommendation) (status not_given))
	(indicator (name number_of_recommendation) (value 0))
	(personality (is nil)))

(defmodule MAIN (export ?ALL))

;==============================================================
;	MAIN:
;	1. A CF is assigned to each activities
;	2. CFs for all user chosen activities are combined
;	3. Combined CFs will be used to infer if user like spicy food
;==============================================================


(defrule MAIN::combine_cf_both_positive
	?x <- (certainty ?cf1&:(> ?cf1 0))
	?y <- (certainty ?cf2&:(> ?cf2 0))
	(test (neq ?x ?y))
		=>
		  (retract ?x)
		  (retract ?y)
		  (assert (certainty (+ ?cf1 (* ?cf2 (- 1.0 ?cf1))))))

(defrule MAIN::combine_cf_1pos_1neg
	?x <- (certainty ?cf1&:(> ?cf1 0))
	?y <- (certainty ?cf2&:(< ?cf2 0))
	(test (neq ?x ?y))
		=>
		  (retract ?x)
		  (retract ?y)
		  (assert (certainty (/(+ ?cf1 ?cf2)(- 1 (min (abs ?cf1) (abs ?cf2)))))))

(defrule MAIN::combine_cf_both_negative
	?x <- (certainty ?cf1&:(< ?cf1 0))
	?y <- (certainty ?cf2&:(< ?cf2 0))
	(test (neq ?x ?y))
		=>
		  (retract ?x)
		  (retract ?y)
		  (assert (certainty (+ ?cf1 (* ?cf2 (+ 1 ?cf1))))))

;==============================================================
;	MAIN:
;	1. Set Conflict Resolution Strategy to 'Random'
; 	2. Set Program Flow DIET -> ALLERGY -> OPEN -> SPICY -> FLAVOR
;==============================================================


(defrule MAIN::start
	(declare (salience -10))
	(indicator (name recommendation) (status not_given))
		=>
			(set-strategy random)
			(focus DIET ALLERGY OPEN SPICY FLAVOR))

;==============================================================
;	Main:
;	1. Recommend up to 3 dishes based on user inputs and inferences
;	2. Priority is given to popular dishes
;==============================================================


(defrule MAIN::recommendation_path_1_pop_3
	(declare
		(salience -100)
		(auto-focus TRUE))
	(is_open yes)
	?x <- (indicator (name recommendation))
	?y <- (sgfood
			(name ?name)
			(is_bizarre yes)
			(calories ?cal)
			(popularity 3)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(<= ?value 3)))
				=>
					(modify ?x (status given))
					(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
					(retract ?y)
					(modify ?z (value (+ ?value 1)))
					(focus MAIN))

(defrule MAIN::recommendation_path_1_pop_2
	(declare
		(salience -200)
		(auto-focus TRUE))
	(is_open yes)
	?x <- (indicator (name recommendation))
	?y <- (sgfood 
			(name ?name)
			(is_bizarre yes)
			(calories ?cal)
			(popularity 2)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
					(modify ?x (status given))
					(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
					(retract ?y)
					(modify ?z (value (+ ?value 1)))
					(focus MAIN)
					)

(defrule MAIN::recommendation_path_1_pop_1
	(declare
		(salience -300)
		(auto-focus TRUE))
	(is_open yes)
	?x <- (indicator (name recommendation))
	?y <- (sgfood 
			(name ?name)
			(is_bizarre yes)
			(calories ?cal)
			(popularity 1)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
					(modify ?x (status given))
					(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
					(retract ?y)
					(modify ?z (value (+ ?value 1)))
					(focus MAIN))

(defrule MAIN::recommendation_path_2_pop_3
	(declare
		(salience -100)
		(auto-focus TRUE))
	(is_spicy yes)
	?x <- (indicator (name recommendation))
	?y <- (sgfood
			(name ?name)
			(is_spicy yes)
			(calories ?cal)
			(popularity 3)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
					(modify ?x (status given))
					(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
					(retract ?y)
					(modify ?z (value (+ ?value 1)))
					(focus MAIN))

(defrule MAIN::recommendation_path_2_pop_2
	(declare
		(salience -200)
		(auto-focus TRUE))
	(is_spicy yes)
	?x <- (indicator (name recommendation))
	?y <- (sgfood
			(name ?name)
			(is_spicy yes)
			(calories ?cal)
			(popularity 2)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
					(modify ?x (status given))
					(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
					(retract ?y)
					(modify ?z (value (+ ?value 1)))
					(focus MAIN))

(defrule MAIN::recommendation_path_2_pop_1
	(declare
		(salience -300)
		(auto-focus TRUE))
	(is_spicy yes)
	?x <- (indicator (name recommendation))
	?y <- (sgfood
			(name ?name)
			(is_spicy yes)
			(calories ?cal)
			(popularity 1)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
					(modify ?x (status given))
					(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
					(retract ?y)
					(modify ?z (value (+ ?value 1)))
					(focus MAIN))

(defrule MAIN::recommendation_path_3_pop_3
	(declare
		(salience -100)
		(auto-focus TRUE))
	(flavor ?fla)
	?x <- (indicator (name recommendation))
	?y <- (sgfood
			(name ?name)
			(flavor ?fla)
			(calories ?cal)
			(popularity 3)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
				(modify ?x (status given))
				(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
				(retract ?y)
				(modify ?z (value (+ ?value 1)))
				(focus MAIN))

(defrule MAIN::recommendation_path_3_pop_2
	(declare
			(salience -200)
			(auto-focus TRUE))
	(flavor ?fla)
	?x <- (indicator (name recommendation))
	?y <- (sgfood
			(name ?name)
			(flavor ?fla)
			(calories ?cal)
			(popularity 2)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
				(modify ?x (status given))
				(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
				(retract ?y)
				(modify ?z (value (+ ?value 1)))
				(focus MAIN))

(defrule MAIN::recommendation_path_3_pop_1
	(declare
		(salience -300)
		(auto-focus TRUE))
	(flavor ?fla)
	?x <- (indicator (name recommendation))
	?y <- (sgfood
			(name ?name)
			(flavor ?fla)
			(calories ?cal)
			(popularity 1)
			(write_up ?wri)
			(image_url ?url))
	?z <- (indicator (name number_of_recommendation) (value ?value&:(< ?value 3)))
				=>
				(modify ?x (status given))
				(printout t "{\"name\":\""?name"\",\"image\":\""?url"\",\"calories\":\""?cal"\",\"write_up\":\""?wri"\"}"crlf)
				(retract ?y)
				(modify ?z (value (+ ?value 1)))
				(focus MAIN))
;==============================================================
;	Main:
;	1. Check if conditions for recommendations are met
;	2. Prompt UI to ask more question if conditions are not met
;==============================================================
(defrule MAIN::selection_comfy_room
(declare (salience -999))
(selection (is comfy_room))
=>
(printout t "Need to ask about activities" crlf))

(defrule MAIN::personality_given
	(declare
		(salience -50)
		(auto-focus TRUE))
	?x <- (personality (is nil))
	?y <- (personality (is ?per&:(neq ?per nil)))
		=>
			(retract ?x)
			(focus DIET ALLERGY OPEN SPICY FLAVOR))

(defrule MAIN::cf_less_than_0
	(declare (salience -999))
	(certainty ?certainty&:(<= ?certainty 0))
	(personality (is nil))
		=>
			(printout t "Need to ask about personality." crlf))

;==============================================================
;	DIET:
;	1. Obtain user's dietary requirements
;	2. Remove unsuitable food from database
;==============================================================

(defmodule DIET (import MAIN ?ALL) (export ?ALL))

(defrule DIET::process_dietreq_halal
	(diet (req $? halal $?))
	?x <- (sgfood (is_hal no))
		=>
			(retract ?x))

(defrule DIET::process_dietreq_vegetarian
	(diet (req $? vegetarian $?))
		=>
			(assert (no_meat beef))
			(assert (no_meat pork))
			(assert (no_meat seafood))
			(assert (no_meat duck))
			(assert (no_meat mutton))
			(assert (no_meat chicken))
			(assert (no_meat seafood)))

(defrule DIET::process_dietreq_kosher
	(diet (req $? kosher $?))
	?x <- (sgfood (is_kos no))
		=>
			(retract ?x))

(defrule DIET::process_no_meat
	(no_meat ?meat)
	?x <- (sgfood (type_of_meat $? ?meat $?))
		=>
			(retract ?x))

;==============================================================
;	ALLERGY:
;	1. Obtain user's allergies
;	2. Remove food with allergens from database
;==============================================================

(defmodule ALLERGY (import MAIN ?ALL) (export ?ALL))

(defrule ALLERGY::process_allergy_gluten
	(allergy (is $? gluten $?))
	?y <- (sgfood (contain_gluten yes))
		=>
			(retract ?y))

(defrule ALLERGY::process_allergy_seafood
	(allergy (is $? seafood $?))
	?y <- (sgfood (contain_seafood yes))
		=>
			(retract ?y))

(defrule ALLERGY::process_allergy_egg
	(allergy (is $? egg $?))
	?y <- (sgfood (contain_egg yes))
		=>
			(retract ?y))

(defrule ALLERGY::process_allergy_peanut
	(allergy (is $? peanut $?))
	?y <- (sgfood (contain_peanut yes))
		=>
			(retract ?y))

;==============================================================
;	OPEN:
;	1. Obtain user's preferred image
;	2. If user is open, recommend bizarre food
;	3. If user is not open (conservative),remove bizarre food from database
;==============================================================

(defmodule OPEN (import MAIN ?ALL) (export ?ALL))


(defrule OPEN::open
	(selection (is sea_horizon))
		=>
		(assert (is_open yes)))

(defrule OPEN::not_open
	(selection (is comfy_room))
	?x <- (sgfood (is_bizarre yes))
		=>
		(assert (is_open no))
		(retract ?x))

;==============================================================
;	SPICY:
;	1. Check combined CF
;	2. If combine CF is > 0, recommend spicy food to user
;==============================================================


(defmodule SPICY (import MAIN ?ALL) (export ?ALL))

(defrule SPICY::spicy
	(certainty ?val&:(> ?val 0))
		=>
			(assert (is_spicy yes)))


;==============================================================
;	FLAVOR:
;	1. Obtain user's personality
;	2. If personality is competitive, recommend savoury food to user
;	3. If personality is helpful, recommend sweet food to user
;	4. If personality is eager, recommend sour food to user
;==============================================================

(defmodule FLAVOR (import MAIN ?ALL) (export ?ALL))

(defrule FLAVOR::competitive
	(personality (is competitive))
		=>
			(assert (competitive yes)))

(defrule FLAVOR::helpful
	(personality (is helpful))
		=>
			(assert (helpful yes)))

(defrule FLAVOR::eager
	(personality (is eager))
		=>
			(assert (eager yes)))

(defrule FLAVOR::flavor_savoury
	(competitive yes)
		=>
			(assert (flavor savoury)))

(defrule FLAVOR::flavor_sweet
	(helpful yes)
		=>
			(assert (flavor sweet)))

(defrule FLAVOR::flavor_sour
	(eager yes)
		=>
			(assert (flavor sour)))
