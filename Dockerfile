FROM python:2.7
ADD . /food-recommendation/
WORKDIR /food-recommendation/
RUN pip install -r requirements.txt
RUN apt update && apt install -y nginx && mv /food-recommendation/nginx.conf /etc/nginx/nginx.conf
