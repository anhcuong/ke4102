setup:
	docker build -t food-recommendation:latest .
start_api:
	docker run --name food-recommendation-backend --restart=always -p 1234:8080 -v $$PWD/:/app -w /app -d food-recommendation:latest bash -c "service nginx start && gunicorn -b 0.0.0.0:8000 main:app"
start_frontend:
	docker run --name food-recommendation-frontend -p 81:80 --restart=always -v $$PWD/:/usr/share/nginx/html:ro -d nginx
destroy:
	docker rm -f food-recommendation-backend
	docker rm -f food-recommendation-frontend
