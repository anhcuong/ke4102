import json

import clips
from flask import request
from flask_api import FlaskAPI
from flask_cors import cross_origin


DATABASE_FILE = 'food_database.clp'
RECOMMENDER_FILE = 'food_recommender.clp'
REQUIRED_CLIPS_FILES = [DATABASE_FILE, RECOMMENDER_FILE]
FACT_FORMAT = '({param} {user_input})'
DIETERY_LIST = ['halal', 'vegetarian', 'kosher']
ALLERGY_LIST = ['gluten', 'seafood', 'egg', 'peanut']
CLIPS_VARIABLES = {
    'diet': 'req',
    'allergy': 'is',
    'selection': 'is',
    'activities': 'certainty',
    'personality': 'is'
}


def _preload_clips_files(preload_files):
    for preload_file in preload_files:
        clips.BatchStar(preload_file)


def _assert_extra_facts(facts):
    for fact in facts:
        print '(assert {})'.format(fact)
        if fact:
            clips.Assert(fact)


def _get_result_from_clips(facts):
    clips.Clear()
    _preload_clips_files(REQUIRED_CLIPS_FILES)
    clips.Reset()
    _assert_extra_facts(facts)
    clips.Run()
    return _format_clips_result(clips.StdoutStream.Read())


def _handle_dietery_requirement(params, dietery_type, dietery_list, nil_keyword):
    param = CLIPS_VARIABLES[dietery_type]
    try:
        if params[nil_keyword]:
            return FACT_FORMAT.format(param=dietery_type, user_input='({} nil)'.format(param))
    except:
        user_input = ''
        for dietery in dietery_list:
            if params.get(dietery):
                user_input += dietery + ' '
        return FACT_FORMAT.format(param=dietery_type, user_input='({} {})'.format(
            param, user_input.strip()))


def _handle_openess(params):
    param = CLIPS_VARIABLES['selection']
    if params['comfyRoom']:
        return FACT_FORMAT.format(param='selection', user_input='({} {})'.format(
            param, 'comfy_room'))
    return FACT_FORMAT.format(param='selection', user_input='({} {})'.format(
        param, 'sea_horizon'))


def _handle_activities(params):
    param = CLIPS_VARIABLES['activities']
    activity_facts = []
    same_fact = set()
    for k, v in params.iteritems():
        if (type(v) == float or type(v) == int) and (v not in same_fact):
            activity_facts.append(FACT_FORMAT.format(param=param, user_input=v))
            same_fact.add(v)
    return activity_facts


def _handle_personality(params):
    param = CLIPS_VARIABLES['personality']
    if 'personality' in params:
        return [FACT_FORMAT.format(param='personality', user_input='({} {})'.format(
            param, params['personality']))]
    return []


def _convert_user_input_to_clips_fact(params):
    facts = []
    facts.append(_handle_dietery_requirement(params, 'diet', DIETERY_LIST, 'hasNoReq'))
    facts.append(_handle_dietery_requirement(params, 'allergy', ALLERGY_LIST, 'hasNoAllergy'))
    facts.append(_handle_openess(params))
    facts = facts + _handle_activities(params) + _handle_personality(params)
    return facts


def _format_clips_result(text_result):
    print text_result
    show_question = -1
    if len(text_result) == 0:
        return show_question, []
    if '{' in text_result:
        rs = []
        recommended_list = text_result.split('\n')
        for recommended_food in recommended_list:
            if len(recommended_food) > 0:
                try:
                    rs.append(json.loads(recommended_food))
                except:
                    continue
        return show_question, rs
    if 'Need' in text_result:
        if 'activities' in text_result:
            show_question = 4
        if 'personality' in text_result:
            show_question = 5
        return show_question, []


app = FlaskAPI(__name__)
@app.route('/recommend', methods=['POST'])
@cross_origin()
def get_recommend():
    params = request.data
    print params
    facts = _convert_user_input_to_clips_fact(params)
    show_question, result = _get_result_from_clips(facts)
    response = {
        'recommended_food': result,
        'show_question': show_question
    }
    print response
    return response
